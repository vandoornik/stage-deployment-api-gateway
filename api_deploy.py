from __future__ import print_function
import sys
import boto3
from botocore.exceptions import ClientError
import os
import uuid

aws_api_id = os.environ.get('AWS_API_ID', '')
stage_name = os.environ.get('BITBUCKET_BRANCH', 'dev')
aws_region = os.environ.get('AWS_DEFAULT_REGION', 'eu-central-1')


def get_account_id():
    try:
        client_sts = boto3.client('sts')
    except ClientError as err:
        print("Failed to create boto3 client.\n" + str(err))
        return False
    return client_sts.get_caller_identity()["Account"]

def set_lambda_permissions():
    account_id = get_account_id()
    if not account_id:
        return False

    try:
        client = boto3.client('lambda')
    except ClientError as err:
        print("Failed to create boto3 client.\n" + str(err))
        return False
    function_list = client.list_functions()
    function_names = [f['FunctionName'] for f in function_list['Functions']]
    for function_name in function_names:
        try:
            response2 = client.add_permission(
                FunctionName=function_name,
                StatementId=str(uuid.uuid4()),
                Action='lambda:InvokeFunction',
                Principal='apigateway.amazonaws.com',
                SourceArn='arn:aws:execute-api:'+aws_region+':'+str(account_id)+':'+aws_api_id+'/*',
                Qualifier=stage_name.upper()
            )
            print(repr(response2))
        except ClientError as err:
            print(repr(err))
    return True

def publish_new_version(api_json_file):
    try:
        client = boto3.client('apigateway')
    except ClientError as err:
        print("Failed to create boto3 client.\n" + str(err))
        return False

    try:
        response = client.put_rest_api(
            restApiId=aws_api_id,
            mode='overwrite',
            failOnWarnings=True,
            body=open(api_json_file, 'rb').read())
        print(response)
    except ClientError as err:
        print("Failed to update rest api.\n" + str(err))
        return False
    except IOError as err:
        print("Failed to access " + api_json_file + ".\n" + str(err))
        return False

    print("create deployment")

    try:
        response = client.create_deployment(
            restApiId=aws_api_id,
            stageName=stage_name,
            description='bitbucket pipelines deployment',
            variables={'alias': stage_name.upper()}
        )
        return response
    except ClientError as err:
        print("Failed to update rest api.\n" + str(err))
        return False


def main():
    if stage_name == 'master':
        print("only branches other than master will be deployed")
    else:
        api_response = publish_new_version('api_gateway_swagger.json')
        if not api_response:
            sys.exit(1)
        if not set_lambda_permissions():
            sys.exit(1)


if __name__ == "__main__":
    main()
